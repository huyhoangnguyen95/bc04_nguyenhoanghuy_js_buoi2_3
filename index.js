
/* Bài 1 :
Input : 
lương 100.000/ngày
người dùng nhập vào 6 ngày

To do :
6 * 100.000

Output :
600.000

*/

function tinhLuong(){
    // console.log("yes");
    var luongNgay = document.getElementById("txt-luong-ngay").value *1;
    var soNgayLam = document.getElementById("txt-so-ngay-lam").value *1;
    
    var result = luongNgay * soNgayLam;
    document.getElementById("resultBai1").innerHTML = `
    ${result}
    `;
}


/* Bài 2 :
Input :
Nhập vào 5 số thực
1.2
2.2
3.3
4.5
6

To do : (1.2 + 2.2 + 3.3 + 4.5 + 6)/5

Output :
3.44

*/

function tinhTrungBinh(){
    // console.log("yes");
    var number1 = document.getElementById("txt-number1").value *1;
    var number2 = document.getElementById("txt-number2").value *1;
    var number3 = document.getElementById("txt-number3").value *1;
    var number4 = document.getElementById("txt-number4").value *1;
    var number5 = document.getElementById("txt-number5").value *1;
    var result = (number1 + number2 + number3 + number4 + number5)/5;
    document.getElementById("resultBai2").innerText = ` ${result} `;
}


/* Bài 3 :
Input :
1$ = 23500 VND
nhập vào số tiền $

To do:
2 * 23500
Output :
47000 VND
*/

function quyDoiTien(){
    // console.log("yes");
    const DOLLAR = 23500;
    var quantity = document.getElementById("txt-quantity").value *1;
    var result  = quantity * DOLLAR;
    document.getElementById("resultBai3").innerHTML = `${result}`;
}


/* Bài 4 :
Input :
Nhập chiều dài : 3
Nhập chiều rộng : 4

To do :
S = dài * rộng
P = (dài + rộng)*2

Output :
S = 12
P = 14

*/

function tinhCongThuc(){
    // console.log("yes");
    var length = document.getElementById("txt-chieu-dai").value *1;
    var width = document.getElementById("txt-chieu-rong").value *1;

    var dienTich = length * width;
    var chuVi = (length + width)*2;
    document.getElementById("resultBai4").innerHTML = `
    Diện tích : ${dienTich}; Chu vi : ${chuVi}
    `;
}


/* Bài 5 :
Input :
Tính tổng 2 kí số : 83

To do : 
8 + 3

Output :
result = 11

*/

function tinhTong(){
    // console.log("yes");
    var n = document.getElementById("txt-so-hai-ki-so").value *1;
    var ten = Math.floor(n/10);
    var unit = n % 10;
    var result = ten + unit;
    document.getElementById("resultBai5").innerHTML = `${result}`;
}